from __future__ import annotations

from typing import Dict

from databuscfg.collection.abstract import ItemProcessor
from databuscfg.profile.entity.abstract import EntityProfile
from databuscfg.profile.entity.abstract import EntityProfileRouteKind
from databuscfg.profile.entity.impl import EntityProfileImpl
from databuscfg.validator.abstract import Validator
from databuscfg.validator.collection.impl import CollectionValidatorImpl
from databuscfg.validator.route.impl import RouteValidatorImpl
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException


class EntityProcessorImpl(ItemProcessor[EntityProfile]):
    _validators: Dict[EntityProfileRouteKind, Validator[str]]

    def __init__(
        self,
        route_validator: RouteValidatorImpl,
        collection_validator: CollectionValidatorImpl
    ) -> None:
        self._validators = {
            EntityProfileRouteKind.ROUTE: route_validator,
            EntityProfileRouteKind.COLLECTION: collection_validator
        }

    def process(
        self,
        unique_id: str,
        tree: Tree
    ) -> EntityProfile:
        try:
            entity = EntityProfileImpl.create(unique_id, tree)

            for route in entity.routes():
                validator = self._validators[route.kind()]
                try:
                    validator.validate(route.unique_id())
                except ExpectedException as e:
                    raise e.wrap(
                        title="{} [{}]".format(
                            route.kind().title(),
                            route.unique_id()
                        )
                    )

            return entity
        except ExpectedException as e:
            raise e.wrap(
                title="Entity [{}]".format(
                    unique_id
                )
            )
