from pygentype.exception import ExpectedException


class ItemAlreadyDefinedException(ExpectedException):
    @staticmethod
    def create(item_name: str, item_id: str) -> ExpectedException:
        return ItemAlreadyDefinedException(
            msg="{} [{}] already defined".format(
                item_name,
                item_id
            )
        )

    def wrap(self, title: str) -> Exception:
        return ItemAlreadyDefinedException(
            msg="{}:\n  {}".format(
                title,
                self.__str__().replace("\n", "\n  ")
            )
        )
