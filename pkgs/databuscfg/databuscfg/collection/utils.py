from typing import Dict
from typing import Iterable
from typing import Tuple

from databuscfg.collection.abstract import ItemProcessor
from databuscfg.collection.abstract import ItemType
from pygentree.abstract import Tree
from pygentype.exception import ExpectedException


def process(
    collection_name: str,
    item_name: str,
    tree_items: Iterable[Tuple[str, Tree]],
    processor: ItemProcessor[ItemType]
) -> Dict[str, ItemType]:
    collection: Dict[str, ItemType] = {}
    try:
        for unique_id, tree in tree_items:
            processed_item = processor.process(unique_id, tree)
            if (unique_id in collection):
                raise ExpectedException(
                    msg="{} [{}] already defined".format(
                        item_name,
                        unique_id
                    )
                )

            collection[unique_id] = processed_item
    except ExpectedException as e:
        raise e.wrap(collection_name)

    return collection
