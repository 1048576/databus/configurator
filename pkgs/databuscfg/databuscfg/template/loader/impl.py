from databuscfg.template.loader.abstract import TemplateLoader
from jinja2 import Template
from pygenpath.abstract import PathResolver


class TemplateLoaderImpl(TemplateLoader):
    _resolver: PathResolver

    def __init__(self, resolver: PathResolver) -> None:
        self._resolver = resolver

    def load(self, name: str) -> Template:
        path = self._resolver.resolve("./{}.md.j2".format(name))
        f = open(path, "r")
        with (f):
            return Template(
                source=f.read()
            )
