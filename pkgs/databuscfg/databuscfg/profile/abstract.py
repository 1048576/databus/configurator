from __future__ import annotations

from typing import Generic
from typing import TypeVar

UniqueId = TypeVar(
    name="UniqueId",
    bound=str
)


class Profile(Generic[UniqueId]):
    def unique_id(self) -> UniqueId:
        ...

    def __lt__(self, other: Profile[UniqueId]) -> bool:
        ...
