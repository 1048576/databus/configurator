from typing import NewType

from databuscfg.profile.abstract import Profile

NodeId = NewType("NodeId", str)


class NodeProfile(Profile[NodeId]):
    ...
