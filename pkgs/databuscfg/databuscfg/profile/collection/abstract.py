from typing import List
from typing import NewType

from databuscfg.profile.abstract import Profile
from databuscfg.profile.route.abstract import RouteId

CollectionId = NewType("CollectionId", str)


class CollectionProfileRoute(Profile[RouteId]):
    ...


class CollectionProfile(Profile[CollectionId]):
    def routes(self) -> List[CollectionProfileRoute]:
        ...
