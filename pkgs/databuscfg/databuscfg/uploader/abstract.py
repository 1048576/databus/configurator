from __future__ import annotations


class Uploader:
    def upload(self, item_id: str, item: str) -> None:
        ...

    def bucket(self, bucket_id: str) -> Uploader:
        ...
