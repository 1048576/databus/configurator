import os

from databuscfg.uploader.abstract import Uploader
from pygenpath.abstract import PathResolver


class UploaderImpl(Uploader):
    @staticmethod
    def create(resolver: PathResolver) -> Uploader:
        os.makedirs(
            name=resolver.pwd(),
            exist_ok=True
        )

        return UploaderImpl(resolver)

    _resolver: PathResolver

    def __init__(self, resolver: PathResolver) -> None:
        self._resolver = resolver

    def upload(self, item_id: str, item: str) -> None:
        path = self._resolver.resolve("./{}".format(item_id))
        f = open(path, "w")
        with (f):
            f.write(item)

    def bucket(self, bucket_id: str) -> Uploader:
        resolver = self._resolver.cd("./{}/".format(bucket_id))

        return UploaderImpl.create(resolver)
