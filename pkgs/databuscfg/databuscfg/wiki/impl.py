import os

from databuscfg.configuration.abstract import Configuration
from databuscfg.configuration.wiki.impl import WikiConfigurationImpl
from databuscfg.template.loader.impl import TemplateLoaderImpl
from databuscfg.uploader.abstract import Uploader
from databuscfg.wiki.abstract import Wiki
from jinja2 import Template
from pygenpath.impl import PathResolverImpl


class WikiImpl(Wiki):
    @staticmethod
    def create() -> Wiki:
        template_resolver = PathResolverImpl.create(
            "{}/templates/".format(os.path.dirname(__file__))
        )
        template_loader = TemplateLoaderImpl(template_resolver)

        home_template = template_loader.load("home")
        entity_template = template_loader.load("entity")

        return WikiImpl(
            home_template=home_template,
            entity_template=entity_template
        )

    _home_template: Template
    _entity_template: Template

    def __init__(
        self,
        home_template: Template,
        entity_template: Template
    ) -> None:
        self._home_template = home_template
        self._entity_template = entity_template

    def generate(
        self,
        configuration: Configuration,
        uploader: Uploader
    ) -> None:
        wiki_configuration = WikiConfigurationImpl(configuration)

        home_rendered = self._home_template.render(
            configuration=wiki_configuration
        )
        uploader.upload("home.md", home_rendered)

        entities_uploader = uploader.bucket("entities")
        for entity in configuration.entities().values():
            entity_rendered = self._entity_template.render(
                entity=entity,
                configuration=wiki_configuration
            )

            entities_uploader.upload(
                item_id="{}.md".format(entity.unique_id()),
                item=entity_rendered
            )
