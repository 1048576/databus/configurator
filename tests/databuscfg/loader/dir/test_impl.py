import os
import shutil
import tempfile

from unittest import skip

from databuscfg.loader.dir.impl import DirLoaderImpl
from pygentestcase.impl import TestCaseImpl
from pygentype.exception import ExpectedException


@skip("")
class DirLoaderTest(TestCaseImpl):
    def test_next_already_loaded(self) -> None:
        err_msg = self.randomText()

        dir_path = tempfile.mkdtemp() + "/"
        try:
            subdir_path = "{}{}/".format(dir_path, self.randomText())
            os.mkdir(subdir_path)

            file_id = self.randomText()
            file_name = file_id + ".yml"

            file1_path = dir_path + file_name
            file2_path = subdir_path + file_name

            for file_path in [file1_path, file2_path]:
                file = open(file_path, "w")
                with (file):
                    file.write("{}\n")

            loader = DirLoaderImpl(dir_path, err_msg)

            err_ctx = self.assertRaises(ExpectedException)
            with (err_ctx):
                for _ in loader:
                    pass

            err_msg_template = (
                "{}:\n"
                "  Cannot process [{}]:\n"
                "    [{}] already loaded from [{}]"
            )

            self.assertEqual(
                first=err_msg_template.format(
                    err_msg,
                    file2_path,
                    file_id,
                    file1_path
                ),
                second=str(err_ctx.exception)
            )
        finally:
            shutil.rmtree(dir_path)

    def test_next_unsupported_file_type(self) -> None:
        err_msg = self.randomText()
        invalid_file_name = self.randomText()

        dir_path = tempfile.mkdtemp() + "/"
        try:
            file_path = dir_path + invalid_file_name
            file = open(file_path, "w")
            with (file):
                file.write("{}\n")

            loader = DirLoaderImpl(dir_path, err_msg)

            err_ctx = self.assertRaises(ExpectedException)
            with (err_ctx):
                for _ in loader:
                    pass

            err_msg_template = (
                "{}:\n"
                "  Cannot process [{}]:\n"
                "    Unsupported file type. Only yaml"
            )
            self.assertEqual(
                first=err_msg_template.format(
                    err_msg,
                    file_path
                ),
                second=str(err_ctx.exception)
            )
        finally:
            shutil.rmtree(dir_path)
