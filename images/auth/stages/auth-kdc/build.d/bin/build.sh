#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "apk add krb5-server krb5-server-ldap"

_sh "rm -rf /var/run/"
_sh "ln -sf /mnt/run /var/run"

_sh "rm -rf /etc/krb5kdc"
_sh "ln -sf /mnt/krb5kdc /etc/krb5kdc"

# Post install
_sh "rm $0"
