FROM python:3.11 as build
WORKDIR /build/
ENV VENV_PATH="./.venv/"
COPY ./images/tests/stages/build/ /
COPY ./images/tests/Dockerfile ./images/tests/Dockerfile
COPY ./.coveragerc ./.coveragerc
COPY ./.flake8 ./.flake8
COPY ./.isort.cfg ./.isort.cfg
COPY ./.pyre_configuration ./.pyre_configuration
COPY ./pyrightconfig.json ./pyrightconfig.json
COPY ./.requirements.txt ./.requirements.txt
COPY ./pkgs/databuscfg/requirements.txt ./pkgs/databuscfg/requirements.txt
RUN /build.d/bin/install.sh
COPY ./pkgs/ ./pkgs/
COPY ./samples/ ./samples/
COPY ./tests/ ./tests/
RUN /build.d/bin/build.sh

FROM build as tests-coverage
RUN psh coverage run -m unittest discover ./

FROM build as tests-flake
RUN psh flake8 ./pkgs/ ./tests/

FROM build as tests-isort
RUN psh isort --check-only ./pkgs/ ./tests/

FROM build as tests-pyre
RUN psh pyre --noninteractive check

FROM build as tests-pyright
RUN psh pyright --stats
