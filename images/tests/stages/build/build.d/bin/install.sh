#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# .pre
csh "python -m venv ${VENV_PATH}"
psh "pip install --upgrade pip"

psh "pip install --requirement ./.requirements.txt"
psh "find ./pkgs/ -mindepth 2 -maxdepth 2 -name requirements.txt | xargs -I {} pip install --requirement {}"

# .post
csh "rm $0"
