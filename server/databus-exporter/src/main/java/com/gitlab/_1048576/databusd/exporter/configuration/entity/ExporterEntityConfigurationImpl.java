package com.gitlab._1048576.databusd.exporter.configuration.entity;

import java.io.IOException;

import com.gitlab._1048576.databusd.configuration.node.ConfigurationObjectNode;
import com.gitlab._1048576.databusd.configuration.processor.ConfigurationProcessor;
import com.gitlab._1048576.databusd.configuration.processor.ConfigurationProcessorImpl;
import com.gitlab._1048576.databusd.exporter.configuration.instance.ExporterInstanceConfiguration;
import com.gitlab._1048576.databusd.exporter.configuration.instance.ExporterInstanceConfigurationImpl;

public class ExporterEntityConfigurationImpl implements ExporterEntityConfiguration {
    private final static ConfigurationProcessor<ExporterInstanceConfiguration> instancesNodeProcessor;

    static {
        instancesNodeProcessor = new ConfigurationProcessorImpl<>(
            ExporterInstanceConfigurationImpl::new
        );
    }

    private final String name;
    private final Iterable<ExporterInstanceConfiguration> instances;

    public ExporterEntityConfigurationImpl(
        final ConfigurationObjectNode node
    ) throws IOException {
        this.name = node.detachTextNode("name");
        this.instances = instancesNodeProcessor.processToUniqueCollection(
            node.detachObjectListNode("instances")
        );
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public Iterable<ExporterInstanceConfiguration> instances() {
        return this.instances;
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof ExporterEntityConfiguration)) {
            return false;
        }

        final ExporterEntityConfiguration other = (ExporterEntityConfiguration) o;

        return this.name.equals(other.name());
    }
}
