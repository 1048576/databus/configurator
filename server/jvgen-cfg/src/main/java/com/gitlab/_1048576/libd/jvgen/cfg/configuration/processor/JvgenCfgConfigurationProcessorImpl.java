package com.gitlab._1048576.libd.jvgen.cfg.configuration.processor;

import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectListNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationProcessor;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public final class JvgenCfgConfigurationProcessorImpl<T> implements JvgenCfgConfigurationProcessor<T> {
    @FunctionalInterface
    public interface ItemFactory<T> {
        T apply(final JvgenCfgConfigurationObjectNode node) throws IOException;
    }

    private final ItemFactory<T> itemFactory;

    public JvgenCfgConfigurationProcessorImpl(
        final ItemFactory<T> itemFactory
    ) {
        this.itemFactory = itemFactory;
    }

    @Override
    public Iterable<T> processToUniqueCollection(
        final JvgenCfgConfigurationObjectListNode node
    ) throws Exception {
        final var collection = new ArrayList<T>();

        while (!node.isEmpty()) {
            final var itemNode = node.detachObjectNode();
            final var item = this.itemFactory.apply(itemNode);

            itemNode.close();

            if (collection.contains(item)) {
                throw new JvgenUserException(
                    String.format(
                        "Duplicated entity found \"%s\"",
                        item.toString()
                    )
                );
            } else {
                collection.add(item);
            }
        }

        return Collections.unmodifiableList(collection);
    }
}
