package com.gitlab._1048576.libd.jvgen.cfg.configuration;

public interface JvgenCfgConfigurationProcessor<T> {
    public Iterable<T> processToUniqueCollection(
        final JvgenCfgConfigurationObjectListNode node
    ) throws Exception;
}
