package com.gitlab._1048576.libd.jvgen.cfg.configuration;

public interface JvgenCfgConfigurationObjectNode {
    public String path();

    public String detachTextNode(
        final String name
    ) throws Exception;

    public boolean detachBooleanNode(
        final String name
    ) throws Exception;

    public JvgenCfgConfigurationObjectNode detachObjectNode(
        final String name
    ) throws Exception;

    public JvgenCfgConfigurationObjectListNode detachObjectListNode(
        final String name
    ) throws Exception;

    public void close() throws Exception;
}
