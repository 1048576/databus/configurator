package com.gitlab._1048576.libd.jvgen.cfg.configuration.jakson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectListNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import java.io.UncheckedIOException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public final class JvgenCfgJaksonConfigurationObjectNodeImpl implements JvgenCfgConfigurationObjectNode {
    private final String path;
    private final ObjectNode jacksonNode;
    private final ObjectMapper mapper;

    public JvgenCfgJaksonConfigurationObjectNodeImpl(
        final String path,
        final JsonNode jacksonNode
    ) throws Exception {
        if (jacksonNode.isObject()) {
            this.path = path;
            this.jacksonNode = (ObjectNode) jacksonNode;
            this.mapper = new ObjectMapper();
        } else {
            throw new JvgenUserException(
                String.format(
                    "Invalid node type %s",
                    path
                )
            );
        }
    }

    @Override
    public String path() {
        return this.path;
    }

    @Override
    public String detachTextNode(
        final String name
    ) throws Exception {
        final var node = this.detachNode(name, JsonNode::isTextual);

        return node.asText();
    }

    @Override
    public boolean detachBooleanNode(
        final String name
    ) throws Exception {
        final var node = this.detachNode(name, JsonNode::isBoolean);

        return node.asBoolean();
    }

    @Override
    public JvgenCfgConfigurationObjectNode detachObjectNode(
        final String name
    ) throws Exception {
        final var node = this.detachNode(name, JsonNode::isObject);

        return new JvgenCfgJaksonConfigurationObjectNodeImpl(
            this.createPath(name),
            node
        );
    }

    @Override
    public JvgenCfgConfigurationObjectListNode detachObjectListNode(
        final String name
    ) throws Exception {
        final var node = this.detachNode(name, JsonNode::isArray);

        return new JvgenCfgJaksonConfigurationObjectListNodeImpl(
            this.createPath(name),
            (ArrayNode) node
        );
    }

    @Override
    public void close() throws Exception {
        if (!this.jacksonNode.isEmpty()) {
            final var nameSpliterator = Spliterators.spliteratorUnknownSize(
                this.jacksonNode.fieldNames(),
                Spliterator.ORDERED
            );
            final var nameStream = StreamSupport.stream(
                nameSpliterator,
                false
            );
            final var pathStream = nameStream.map(
                this::createPath
            );

            throw new JvgenUserException(
                String.format(
                    "Unexpected nodes {%s}",
                    pathStream.collect(Collectors.joining(", "))
                )
            );
        }
    }

    private String createPath(final String name) {
        try {
            return String.format(
                "%s.[%s]",
                this.path,
                this.mapper.writeValueAsString(name)
            );
        } catch (final JsonProcessingException e) {
            throw new UncheckedIOException(e);
        }
    }

    private JsonNode detachNode(
        final String name,
        final Function<JsonNode, Boolean> validate
    ) throws Exception {
        final var jacksonNode = this.jacksonNode.remove(name);

        if (jacksonNode == null) {
            throw new JvgenUserException(
                String.format(
                    "Node not found %s",
                    this.createPath(name)
                )
            );
        }

        if (!validate.apply(jacksonNode)) {
            throw new JvgenUserException(
                String.format(
                    "Invalid node type %s",
                    this.createPath(name)
                )
            );
        }

        return jacksonNode;
    }
}
