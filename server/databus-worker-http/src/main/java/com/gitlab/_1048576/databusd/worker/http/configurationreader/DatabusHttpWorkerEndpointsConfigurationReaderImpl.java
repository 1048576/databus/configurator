package com.gitlab._1048576.databusd.worker.http.configurationreader;

import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerEndpointConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerEndpointsConfiguration;
import com.gitlab._1048576.databusd.worker.http.DatabusHttpWorkerEndpointsConfigurationReader;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectListNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DatabusHttpWorkerEndpointsConfigurationReaderImpl implements DatabusHttpWorkerEndpointsConfigurationReader {
    @Override
    public DatabusHttpWorkerEndpointsConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var endpointsNode = node.detachObjectListNode(
            "endpoints"
        );

        final var endpoints = this.createEndpoints(
            endpointsNode
        );

        endpointsNode.close();

        return new DatabusHttpWorkerEndpointsConfiguration() {
            @Override
            public Map<String, DatabusHttpWorkerEndpointConfiguration> endpoints() {
                return Collections.unmodifiableMap(endpoints);
            }
        };
    }

    private Map<String, DatabusHttpWorkerEndpointConfiguration> createEndpoints(
        final JvgenCfgConfigurationObjectListNode node
    ) throws Exception {
        final var endpoints = new HashMap<String, DatabusHttpWorkerEndpointConfiguration>();

        while (!node.isEmpty()) {
            final var endpointNode = node.detachObjectNode();

            final var endpoint = this.createEndpoint(endpointNode);

            endpointNode.close();

            if (endpoints.put(endpoint.name(), endpoint) != null) {
                throw new JvgenUserException(
                    String.format(
                        "Endpoint [%s] is not unique",
                        endpoint.name()
                    )
                );
            }
        }

        return endpoints;
    }

    private DatabusHttpWorkerEndpointConfiguration createEndpoint(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var name = node.detachTextNode("name");
        final var url = node.detachTextNode("url");
        final var keytab = node.detachTextNode("keytab");

        return new DatabusHttpWorkerEndpointConfiguration() {
            @Override
            public String name() {
                return name;
            }

            @Override
            public String url() {
                return url;
            }

            @Override
            public String keytab() {
                return keytab;
            }
        };
    }
}
