package com.gitlab._1048576.databusd.worker.http;

import com.gitlab._1048576.databusd.worker.handler.rabbitmq.DatabusWorkerRabbitmqQueueHandlerConfiguration;
import java.util.Map;

public interface DatabusHttpWorkerRabbitmqHandlersConfigurationReader {
    public DatabusHttpWorkerRabbitmqHandlersConfiguration read(
        final Iterable<DatabusWorkerRabbitmqQueueHandlerConfiguration> queues,
        final Map<String, DatabusHttpWorkerEndpointConfiguration> endpoints,
        final Map<String, DatabusHttpWorkerKeytabConfiguration> keytabs
    ) throws Exception;
}
