package com.gitlab._1048576.libd.jvgen.kerberos.configurationreader;

import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.kerberos.JvgenKerberosKeytabConfiguration;
import com.gitlab._1048576.libd.jvgen.kerberos.JvgenKerberosKeytabConfigurationReader;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Base64;

public class JvgenKerberosKeytabConfigurationReaderImpl implements JvgenKerberosKeytabConfigurationReader {
    @Override
    public JvgenKerberosKeytabConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var principal = node.detachTextNode(
            "principal"
        );
        final var keytabBase64 = node.detachTextNode(
            "keytab-base64"
        );

        final var filepath = Files.createTempFile(
            Paths.get("/tmp/"),
            null,
            ".keytab",
            PosixFilePermissions.asFileAttribute(
                PosixFilePermissions.fromString("rw-------")
            )
        );

        final var outputStream = new FileOutputStream(
            filepath.toString()
        );

        try (outputStream) {
            outputStream.write(Base64.getDecoder().decode(keytabBase64));
        }

        return new JvgenKerberosKeytabConfiguration() {
            @Override
            public String principal() {
                return principal;
            }

            @Override
            public String filepath() {
                return filepath.toString();
            }
        };
    }
}
