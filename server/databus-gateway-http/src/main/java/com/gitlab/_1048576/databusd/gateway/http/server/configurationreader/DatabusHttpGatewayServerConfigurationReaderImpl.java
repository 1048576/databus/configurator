package com.gitlab._1048576.databusd.gateway.http.server.configurationreader;

import com.gitlab._1048576.databusd.gateway.http.DatabusHttpGatewayServerConfiguration;
import com.gitlab._1048576.databusd.gateway.http.DatabusHttpGatewayServerConfigurationReader;
import com.gitlab._1048576.databusd.gateway.http.server.security.DatabusHttpGatewayServerSecurityUserRole;
import com.gitlab._1048576.databusd.gateway.http.server.security.userdetails.DatabusHttpGatewayServerSecuritySimpleUserDetailsServiceImpl;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectListNode;
import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.httpserver.security.JvgenHttpServerSecurityAuthenticator;
import com.gitlab._1048576.libd.jvgen.httpserver.security.authenticator.JvgenHttpServerSecurityFakeAuthenticatorImpl;
import com.gitlab._1048576.libd.jvgen.httpserver.security.authenticator.JvgenHttpServerSecurityKerberosAuthenticatorImpl;
import com.gitlab._1048576.libd.jvgen.kerberos.configurationreader.JvgenKerberosKeytabConfigurationReaderImpl;
import java.util.ArrayList;
import java.util.Collections;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;

public class DatabusHttpGatewayServerConfigurationReaderImpl implements DatabusHttpGatewayServerConfigurationReader {
    @Override
    public DatabusHttpGatewayServerConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var securityNode = node.detachObjectNode(
            "security"
        );

        final var authenticator = this.createAuthenticator(securityNode);

        securityNode.close();

        return new DatabusHttpGatewayServerConfiguration() {
            @Override
            public JvgenHttpServerSecurityAuthenticator createAuthenticator() {
                return authenticator;
            }
        };
    }

    private JvgenHttpServerSecurityAuthenticator createAuthenticator(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var securityEnable = node.detachBooleanNode(
            "enable"
        );

        if (securityEnable) {
            final var usersNode = node.detachObjectListNode(
                "users"
            );
            final var kerberosNode = node.detachObjectNode(
                "kerberos"
            );

            final var userDetailsService = this.createUserDetailsService(
                usersNode
            );
            final var authenticator = this.createKerberosAuthenticator(
                userDetailsService,
                kerberosNode
            );

            usersNode.close();
            kerberosNode.close();

            return authenticator;
        } else {
            return new JvgenHttpServerSecurityFakeAuthenticatorImpl(
                Collections.singletonList(
                    new SimpleGrantedAuthority(DatabusHttpGatewayServerSecurityUserRole.FULL.name())
                )
            );
        }
    }

    private JvgenHttpServerSecurityAuthenticator createKerberosAuthenticator(
        final UserDetailsService userDetailsService,
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var configurationReader = new JvgenKerberosKeytabConfigurationReaderImpl();

        final var configuration = configurationReader.read(node);

        return new JvgenHttpServerSecurityKerberosAuthenticatorImpl(
            configuration,
            userDetailsService
        );
    }

    private UserDetailsService createUserDetailsService(
        final JvgenCfgConfigurationObjectListNode node
    ) throws Exception {
        final var users = new ArrayList<String>();

        while (!node.isEmpty()) {
            users.add(
                node.detachObjectNode().detachTextNode("username")
            );
        }

        return new DatabusHttpGatewayServerSecuritySimpleUserDetailsServiceImpl(users);
    }
}
