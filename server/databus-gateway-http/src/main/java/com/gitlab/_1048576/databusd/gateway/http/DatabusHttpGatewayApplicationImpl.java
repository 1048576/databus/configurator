package com.gitlab._1048576.databusd.gateway.http;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabusHttpGatewayApplicationImpl {
    public static void main(final String[] args) {
        SpringApplication.run(DatabusHttpGatewayApplicationImpl.class, args);
    }
}
