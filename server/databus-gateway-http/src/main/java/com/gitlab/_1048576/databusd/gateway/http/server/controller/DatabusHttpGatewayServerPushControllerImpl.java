package com.gitlab._1048576.databusd.gateway.http.server.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab._1048576.databusd.gateway.http.DatabusHttpGatewayProcessor;
import com.gitlab._1048576.databusd.logging.logger.DatabusLogger;
import com.gitlab._1048576.libd.jvgen.core.JvgenUserException;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DatabusHttpGatewayServerPushControllerImpl {
    private static final String KEY_TRACKING_ID = "trackingId";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_ERROR = "error";

    private final DatabusHttpGatewayProcessor processor;
    private final DatabusLogger databusLogger;
    private final ObjectMapper objectMapper;
    private final Logger logger;

    public DatabusHttpGatewayServerPushControllerImpl(
        final DatabusHttpGatewayProcessor processor,
        final DatabusLogger databusLogger
    ) {
        this.processor = processor;
        this.databusLogger = databusLogger;
        this.objectMapper = new ObjectMapper();
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    @PostMapping(
        path = "/push/{routingKey}",
        produces = "application/json"
    )
    public ResponseEntity<String> processPushRequest(
        @PathVariable
        final String routingKey,
        final HttpEntity<byte[]> httpEntity,
        final HttpServletRequest httpServletRequest
    ) throws JsonProcessingException {
        final var responseBody = this.objectMapper.createObjectNode();

        final var headers = this.createHeaders(
            routingKey,
            httpServletRequest
        );

        try {
            final var body = this.createBody(httpEntity);
            final var contentType = this.createContentType(httpEntity);

            final var trackingId = this.processor.process(
                body,
                contentType,
                routingKey,
                headers
            );

            responseBody.put(KEY_TRACKING_ID, trackingId);
            responseBody.put(KEY_SUCCESS, true);

            this.databusLogger.messageProcessingCompleted(
                trackingId,
                routingKey,
                headers,
                body.length
            );

            return ResponseEntity.ok().body(
                this.objectMapper.writeValueAsString(responseBody)
            );
        } catch (final JvgenUserException e) {
            responseBody.put(KEY_SUCCESS, false);
            responseBody.put(KEY_ERROR, e.getMessage());

            this.databusLogger.messageProcessingFailed(
                routingKey,
                headers,
                e.getMessage()
            );

            return ResponseEntity.internalServerError().body(
                this.objectMapper.writeValueAsString(responseBody)
            );
        } catch (final Exception e) {
            this.logger.error("Unreachable statement", e);

            responseBody.put(KEY_SUCCESS, false);

            return ResponseEntity.internalServerError().body(
                this.objectMapper.writeValueAsString(responseBody)
            );
        }
    }

    private String createContentType(
        final HttpEntity<byte[]> httpEntity
    ) throws Exception {
        final var contentType = httpEntity.getHeaders().getContentType();

        if (contentType == null) {
            throw new JvgenUserException("Empty content type");
        } else {
            return String.format(
                "%s/%s",
                contentType.getType(),
                contentType.getSubtype()
            );
        }
    }

    private byte[] createBody(
        final HttpEntity<byte[]> httpEntity
    ) throws Exception {
        if (httpEntity.getBody() == null) {
            throw new JvgenUserException("Empty body");
        } else {
            return httpEntity.getBody();
        }
    }

    private Map<String, String> createHeaders(
        final String routingKey,
        final HttpServletRequest httpServletRequest
    ) {
        final var parameterMap = httpServletRequest.getParameterMap();

        final var parameterStream = parameterMap.entrySet().stream();

        final var result = parameterStream.collect(
            Collectors.toMap(
                (entry) -> {
                    return entry.getKey();
                },
                (entry) -> {
                    return String.join(",", entry.getValue());
                }
            )
        );

        result.put("routingKey", routingKey);

        return result;
    }
}
