package com.gitlab._1048576.libd.jvgen.rabbitmq.configurationreader;

import com.gitlab._1048576.libd.jvgen.cfg.configuration.JvgenCfgConfigurationObjectNode;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBroker;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBrokerConfiguration;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBrokerConfigurationReader;
import com.gitlab._1048576.libd.jvgen.rabbitmq.JvgenRabbitmqBrokerImpl;
import java.net.URI;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory.ConfirmType;

public class JvgenRabbitmqBrokerConfigurationReaderImpl implements JvgenRabbitmqBrokerConfigurationReader {
    @Override
    public JvgenRabbitmqBrokerConfiguration read(
        final JvgenCfgConfigurationObjectNode node
    ) throws Exception {
        final var uri = node.detachTextNode("uri");
        final var username = node.detachTextNode("username");
        final var password = node.detachTextNode("password");

        final var connectionFactory = new CachingConnectionFactory(
            new URI(uri)
        );

        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setPublisherReturns(true);
        connectionFactory.setPublisherConfirmType(ConfirmType.CORRELATED);

        return new JvgenRabbitmqBrokerConfiguration() {
            public JvgenRabbitmqBroker create() throws Exception {
                return new JvgenRabbitmqBrokerImpl(
                    connectionFactory
                );
            }
        };
    }
}
