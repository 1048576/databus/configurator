package com.gitlab._1048576.libd.jvgen.rabbitmq;

import java.util.Map;

public interface JvgenRabbitmqPublisher {
    public String publish(
        final byte[] body,
        final String contentType,
        final String routingKey,
        final Map<String, ? extends Object> headers
    ) throws Exception;
}
